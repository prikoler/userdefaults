import UIKit

class ViewController: UIViewController {

    private let backgroundKey = "backgroundColor"

    override func viewDidLoad() {
        super.viewDidLoad()

        setupButton()
        loadBackgroundColor()
    }

    private func setupButton() {
        let button = UIButton(type: .system)
        button.setTitle("Change Background", for: .normal)
        button.addTarget(self, action: #selector(changeBackgroundColor), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor.white
        button.layer.cornerRadius = 10.0
        button.contentEdgeInsets = UIEdgeInsets(top: 8, left: 10, bottom: 8, right: 10)
        view.addSubview(button)

        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

    private func loadBackgroundColor() {
        if let colorData = UserDefaults.standard.data(forKey: backgroundKey),
           let color = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(colorData) as? UIColor {
            view.backgroundColor = color
        } else {
            view.backgroundColor = .white
        }
    }

    private func randomColor() -> UIColor {
        let red = CGFloat(arc4random_uniform(256)) / 255.0
        let green = CGFloat(arc4random_uniform(256)) / 255.0
        let blue = CGFloat(arc4random_uniform(256)) / 255.0

        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }


    @objc private func changeBackgroundColor() {
        let color = randomColor()
        view.backgroundColor = color

        if let colorData = try? NSKeyedArchiver.archivedData(withRootObject: color, requiringSecureCoding: false) {
            UserDefaults.standard.set(colorData, forKey: backgroundKey)
        }
    }
}
